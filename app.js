require('dotenv-extended').load();

const builder = require('botbuilder'),
    restify = require('restify'),
    bbPromise = require('bluebird'),
    request = require('request-promise').defaults({encoding: null}),
    utils = require('./utils');

// Setup Restify Server
const server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function () {
   console.log('%s listening to %s', server.name, server.url); 
});

// Create chat connector for communicating with the Bot Framework Service
const connector = new builder.ChatConnector({
    appId: process.env.MICROSOFT_APP_ID,
    appPassword: process.env.MICROSOFT_APP_PASSWORD,
    gzipData: true
});

// Listen for messages from users 
server.post('/api/messages', connector.listen());

// See if user sent image and try to process
let bot = new builder.UniversalBot(connector);
// Create recognizer from LUIS model
const recognizer = new builder.LuisRecognizer(process.env.MTL_LUIS_MODEL);
bot.recognizer(recognizer);

bot.dialog('/', [
    function (session, args, next) {
        let msg = session.message;
        if (msg.attachments.length) {
            let attachment = msg.attachments[0];
            // verify the attachment is an image
            if (!attachment.contentType.match(utils.phrases.IMAGE_REGEX)) {
                session.endDialog(utils.phrases.NOT_AN_IMAGE_ATTACHMENT);
            } else {
                session.dialogData.imageUrl = attachment.contentUrl;
                // check if we know the gender preference, if not, get it
                if (!session.userData.genderPreference) {
                    builder.Prompts.choice(session, 
                        utils.phrases.PREF_MALE_FEMALE,
                        utils.phrases.OPTIONS_MALE_FEMALE
                    );
                } else {
                    next({response: session.userData.genderPreference})
                }
            }
        } else {
            session.send(utils.phrases.TRY_HELP_MSG, session.message.text);
        }
    },
    function (session, results, next) {
        let imageUrl = session.dialogData.imageUrl;
        let gender = results.response.entity;
        
        // Send request to MTL mirrorlook endpoint
        utils.mtlAapi.mtlmirrorthatlook(
            process.env.MTL_API_ENDPOINT,
            imageUrl,
            gender,
            process.env.MTL_API_SUBSCRIPTION_KEY
        ).then(function (jsondata) {
            // Save jsondata for later use (makes session too large if gzipdata is unset)
             session.privateConversationData.lastMirrorCallResults = jsondata;

            // See if user want tops or bottoms
            let clothingTypes = Object.keys(jsondata.result);
            builder.Prompts.choice(session,
                utils.phrases.PREF_CLOTHING_TYPES,
                clothingTypes
            );
        }).catch(function(err) {
            // We had an error making the call -- send to error dialog
            console.log('error in api call', err.message);
            session.replaceDialog('Error');
        });
    }, 
    function(session, results) {
        let clothingType = results.response.entity;
        session.replaceDialog('Browse', {clothingType: clothingType, cursor:0});
    }
]);

// Handle Greetings
bot.dialog('Greet', [
    function (session, args, next) {
        session.endDialog(utils.phrases.GREETING);
    }
]).triggerAction({
    matches: 'Greet'
});

// Handle help and give instructions on what user can do
bot.dialog('Help', [
    function (session, args, next) {
        session.send(utils.phrases.HELP_TEXT);
        next();
    }
]).triggerAction({
    matches: 'Help'
});

// Handle when there is an error
bot.dialog('Error', function (session) {
    session.endDialog(utils.phrases.ERROR_TEXT);
});

// Handle when browsing based on prefereces
bot.dialog('Browse', [
    function (session, args, next) {
        // Display 10 items at a time based on set preferences
        let products = session.privateConversationData.lastMirrorCallResults
            .result[args.clothingType]['products'];
        let carouselMessage = utils.carousel.createCarouselFromProducts(
            session,
            builder,
            products.slice(args.cursor, utils.MAX_DISPLAY_CARDS)
        );
        
        session.dialogData.clothingType = args.clothingType;
        session.dialogData.cursor =  Math.min(args.cursor + utils.MAX_DISPLAY_CARDS,
            products.length);
        session.dialogData.productsLength = products.length
        
        session.send(carouselMessage);
        
        next();
    },
    function (session) {
        // Build options for what user can do now
        let options = []
        let cursor = session.dialogData.cursor;
        let productsLength = session.dialogData.productsLength;
        
        // 1. See next 10 if we have more
        if (cursor < productsLength - 1) options.push(utils.phrases.NEXT_TEN);
        // 2. See prev 10 if we have more
        if (cursor > 9) options.push(utils.phrases.PREV_TEN);
        // 3. Change clothings type (tops, bottom)
        options.push(utils.phrases.CHANGE_CLOTHING_TYPE);
        // 4. Search for something else
        options.push(utils.phrases.SEARCH_OPTION);
        // 5. Change search preferences
        options.push(utils.phrases.CHANGE_SEARCH_PREFS);
        // 6. User is done for now
        options.push(utils.phrases.DONE);

        builder.Prompts.choice(session,
            utils.phrases.BROWSE_NEXT_STEPS,
            options
        )
    },
    function (session, results) {
        let choice = results.response.entity;
        let products = session.privateConversationData.lastMirrorCallResults
            .result[session.dialogData.clothingType]['products'];
        let clothingType = session.dialogData.clothingType;
        let cursor = session.dialogData.cursor;

        switch(choice) {
            case utils.phrases.NEXT_TEN:
                session.replaceDialog('Browse', {clothingType: clothingType,
                    cursor: cursor})
                break;
            case utils.phrases.PREF_TEN:
                session.replaceDialog('Browse',{clothingType: clothingType,
                    cursor: cursor-utils.MAX_DISPLAY_CARDS})
                break;
            case utils.phrases.CHANGE_CLOTHING_TYPE:
                session.replaceDialog('SelectClothingType');
                break;
            case utils.phrases.SEARCH_OPTION:
                session.send(utils.phrases.SEARCH_OPTION_TEXT);
                session.endDialog();
                break;
            case utils.phrases.CHANGE_SEARCH_PREFS:
                session.replaceDialog('Help');
                break;
            case utils.phrases.DONE:
                session.replaceDialog('EndConversation');
        }
    }
]);

bot.dialog('SelectClothingType', [
    function (session) {
        // Get the clothing type options
        let options = Object.keys(session.privateConversationData.lastMirrorCallResults
            .result);

        builder.Prompts.choice(session,
            utils.phrases.CLOTHING_TYPE_TEXT,
            options
        )
    },
    function (session, results) {
        let clothingType = results.response.entity;
        session.replaceDialog('Browse', {clothingType: clothingType,
            cursor: 0});
    }
]);

bot.dialog('EndConversation', function (session) {
    session.endConversation(utils.phrases.DONE_RESPONSE)
}).triggerAction({
    matches: 'EndConversation'
})