/**
 * Phrase and responses that the bot will use
 */

const GREETING = `Hi! Start shopping by sending us a picture of an outfit
    you like. Type 'help' at any point to see what else you can do. Happy
    shopping.`;

const NOT_AN_IMAGE_ATTACHMENT = `The attachment you sent is not an image. Try
    sending an image attachment instead.`;

const PREF_MALE_FEMALE = `Do you want to see results for men or women?`;
const OPTIONS_MALE_FEMALE = `Male|Female`;
const TRY_HELP_MSG = `Sorry, I did not understand \'%s\'. Type \'help\' if you need assistance.`;
const HELP_TEXT = `Help is on the way`;
const PREF_CLOTHING_TYPES = `We found lots of great options. Where do you want to start?`;
const BROWSE_NEXT_STEPS = `What would you like to do next?`;
const IMAGE_REGEX = /image\/\s*/;

const NEXT_TEN = `See next 10 items`;
const PREV_TEN = `See previous 10 items`;
const CHANGE_CLOTHING_TYPE = `Change clothing type`;
const SEARCH_OPTION = `Search for something else`;
const SEARCH_OPTION_TEXT = `Sure, send me another image when you are ready`;
const CHANGE_SEARCH_PREFS = `Change search preferences`;
const CLOTHING_TYPE_TEXT = `What type of clothing are you interested in?`;
const DONE = `None of these. I'm done for now`;
const DONE_RESPONSE = `No problem. I hope to see you again soon. Bye.`;

const ERROR_TEXT = `Woah! Seems my brain is spinning today and I can't figure out
    what's going on. I'm going to take a nap. Let's talk in a few hours :)`;

exports.GREETING = GREETING;
exports.NOT_AN_IMAGE_ATTACHMENT = NOT_AN_IMAGE_ATTACHMENT;
exports.PREF_MALE_FEMALE = PREF_MALE_FEMALE;
exports.OPTIONS_MALE_FEMALE = OPTIONS_MALE_FEMALE;
exports.TRY_HELP_MSG = TRY_HELP_MSG;
exports.PREF_CLOTHING_TYPES = PREF_CLOTHING_TYPES;
exports.BROWSE_NEXT_STEPS = BROWSE_NEXT_STEPS;
exports.IMAGE_REGEX = IMAGE_REGEX;
exports.NEXT_TEN = NEXT_TEN;
exports.PREV_TEN = PREV_TEN;
exports.CHANGE_CLOTHING_TYPE = CHANGE_CLOTHING_TYPE;
exports.SEARCH_OPTION = SEARCH_OPTION;
exports.SEARCH_OPTION_TEXT = SEARCH_OPTION_TEXT;
exports.CHANGE_SEARCH_PREFS =CHANGE_SEARCH_PREFS;
exports.CLOTHING_TYPE_TEXT = CLOTHING_TYPE_TEXT;
exports.DONE = DONE;
exports.DONE_RESPONSE = DONE_RESPONSE;
exports.ERROR_TEXT = ERROR_TEXT;
exports.HELP_TEXT = HELP_TEXT;