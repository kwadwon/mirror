/**
 * Utility file to help with the creation of hero cards from 
 * individual products from MTL API
 */

function createHeroCardFromProduct (session, builder, productInfo) {
    let title = productInfo.name;
    let price = productInfo.price;
    let salePrice = productInfo.salePrice || "--";
    let retailer = productInfo.retailer;
    let description = productInfo.description;
    let url = productInfo.clickUrl;
    let imageUrl = productInfo.images[0];
    let subtitle = `${productInfo.retailer}, $${price}`

    return new builder.HeroCard(session)
        .title(title)
        .subtitle(subtitle)
        .text(description)
        .images([
            builder.CardImage.create(session, imageUrl)
        ])
        .buttons([
            builder.CardAction.openUrl(session, url)
        ]);
}

exports.createHeroCardFromProduct = createHeroCardFromProduct;