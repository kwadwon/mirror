/**
 * Wrapper file for calls to MTL API
 */

const request = require('request-promise').defaults({encoding: null});

function mtlmirrorthatlook (base_url, imageUrl, gender, api_key) {
    let fullURL = `${base_url}image=${imageUrl}&gender=${gender}`;
    let options = {
        uri: fullURL,
        headers: {
            'Ocp-Apim-Subscription-Key': api_key
        },
        json: true
    };
    return request(options);
};

exports.mtlmirrorthatlook = mtlmirrorthatlook;

 